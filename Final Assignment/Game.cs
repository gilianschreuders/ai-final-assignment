﻿using System;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace Final_Assignment
{
    public partial class Game : Form
    {
        /*=====================================================================
         _variables
         =====================================================================*/
        public static readonly World World = new World();
        public static readonly Random GameRandom = new Random();
        /*=====================================================================
         Constructors
         =====================================================================*/

        public Game()
        {
            InitializeComponent();

            ViewPort = new ViewPort(this);

            IsDone = true;

            //Set timer
            var timer = new Timer();
            timer.Elapsed += Run;
            timer.Interval = 16.68; //60 FPS
            timer.Enabled = true;
            Time = DateTime.Now;
        }

        /*=====================================================================
         Properties
         =====================================================================*/
        //Arrow Keys
        public bool UpKey { get; private set; }
        public bool DownKey { get; private set; }
        public bool LeftKey { get; private set; }
        public bool RightKey { get; private set; }
        //Drawable graph related
        public bool ShowGraph { get; private set; }
        public bool ShowCalculations { get; private set; }
        public bool ShowPath { get; private set; }
        //Drawable graph related
        public bool ShowGoals { get; private set; }
        //Others
        public ViewPort ViewPort { get; private set; }
        public DateTime Time { get; private set; }
        //Loop
        public bool IsDone { get; private set; }
        /*=====================================================================
         Methods
         =====================================================================*/

        private void KeyLogic()
        {
            throw new NotImplementedException();

            Invoke((Action) (() =>
            {
                if (UpKey)
                {
                    ViewPort.Up();
                }
                if (DownKey)
                {
                    ViewPort.Down();
                }
                if (LeftKey)
                {
                    ViewPort.Left();
                }
                if (RightKey)
                {
                    ViewPort.Right();
                }
            }));
        }

        private void Logic(DateTime signalTime)
        {
            var timeElapsed = (signalTime - Time).Milliseconds;

            foreach (var baseGameEntity in World.MovingEntities)
            {
                baseGameEntity.Update(timeElapsed);
            }

            Time = signalTime;
        }

        private void Draw()
        {
            Canvas.Invalidate();
        }

        /*=====================================================================
         Events
         =====================================================================*/

        private void Run(object source, ElapsedEventArgs e)
        {
            if (IsDone)
            {
                IsDone = false;
                Logic(e.SignalTime);
                Draw();
                IsDone = true;
            }
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            //Non painting:
            if (UpKey) ViewPort.Up();
            if (DownKey) ViewPort.Down();
            if (LeftKey) ViewPort.Left();
            if (RightKey) ViewPort.Right();

            //Graph related
            if (ShowGraph) World.Graph.Draw(e.Graphics);
            if (ShowCalculations) World.Graph.DrawCalculations(e.Graphics);
            if (ShowPath) World.Character.SteeringBehaviours.DrawPath(e.Graphics);

            //Draw entities
            foreach (var movingEntity in World.DrawableEntities) movingEntity.Renderer(e.Graphics);

            //Goal related
            if (ShowGoals) World.Character.Goals.Draw(e.Graphics);
        }

        private void Game_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) Close();

            //F keys
            if (e.KeyCode == Keys.F1) ShowGraph = !ShowGraph;
            if (e.KeyCode == Keys.F2) ShowCalculations = !ShowCalculations;
            if (e.KeyCode == Keys.F3) ShowPath = !ShowPath;
            if (e.KeyCode == Keys.F4) ShowGoals = !ShowGoals;

            //Arrow keys
            if (e.KeyCode == Keys.Left) LeftKey = true;
            if (e.KeyCode == Keys.Right) RightKey = true;
            if (e.KeyCode == Keys.Up) UpKey = true;
            if (e.KeyCode == Keys.Down) DownKey = true;
        }

        private void Game_KeyUp(object sender, KeyEventArgs e)
        {
            //Arrow keys
            if (e.KeyCode == Keys.Left) LeftKey = false;
            if (e.KeyCode == Keys.Right) RightKey = false;
            if (e.KeyCode == Keys.Up) UpKey = false;
            if (e.KeyCode == Keys.Down) DownKey = false;
        }

        private void Canvas_Click(object sender, EventArgs e)
        {
            var mousePosition = Canvas.PointToClient(Cursor.Position);
            var character = World.Character;
            character.MoveTo(mousePosition.X, mousePosition.Y);
        }
    }
}
﻿using Final_Assignment.Entities;
using Final_Assignment.Fuzzy;

namespace Final_Assignment.Evaluators
{
    public class RecoverGrade : CharacterGrade
    {
        /*===========================================================================
         Constructors
         ===========================================================================*/

        public RecoverGrade(Character character) : base(character)
        {
            FzSet lowExhausted;
            FzSet mediumExhausted;
            FzSet highExhausted;

            FuzzyVariable = FuzzyModule.CreateFLV("recover");

            lowExhausted = FuzzyVariable.AddLeftShoulderSet("low", 0, 50, 25);
            mediumExhausted = FuzzyVariable.AddTriangularSet("medium", 25, 75, 50);
            highExhausted = FuzzyVariable.AddRightShoulderSet("high", 50, 100, 75);

            FuzzyModule.AddRule(new FzVery(lowExhausted), VeryDesirable);
            FuzzyModule.AddRule(new FzVery(mediumExhausted), Desirable);
            FuzzyModule.AddRule(new FzVery(highExhausted), Undesirable);
        }

        /*===========================================================================
         Methods
         ===========================================================================*/

        public override float GetDesirability()
        {
            float result;

            FuzzyVariable.Fuzzify(Character.Exhausted);
            result = FuzzyModule.Defuzzify("desirability");

            return result;
        }
    }
}
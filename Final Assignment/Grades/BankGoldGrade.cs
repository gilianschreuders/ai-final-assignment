﻿using Final_Assignment.Entities;
using Final_Assignment.Fuzzy;

namespace Final_Assignment.Evaluators
{
    public class BankGoldGrade : CharacterGrade
    {
        /*===========================================================================
         Constructors
         ===========================================================================*/

        public BankGoldGrade(Character character)
            : base(character)
        {
            FzSet lowGold;
            FzSet mediumGold;
            FzSet highGold;

            FuzzyVariable = FuzzyModule.CreateFLV("bank");

            lowGold = FuzzyVariable.AddLeftShoulderSet("low", 0, 50, 75);
            mediumGold = FuzzyVariable.AddTriangularSet("medium", 25, 75, 50);
            highGold = FuzzyVariable.AddRightShoulderSet("high", 500, int.MaxValue, 600);

            FuzzyModule.AddRule(new FzFairly(lowGold), VeryDesirable);
            FuzzyModule.AddRule(new FzFairly(mediumGold), Desirable);
            FuzzyModule.AddRule(new FzFairly(highGold), Undesirable);
        }

        /*===========================================================================
         Methods
         ===========================================================================*/

        public override float GetDesirability()
        {
            float result;

            FuzzyVariable.Fuzzify(Character.Gold);
            result = FuzzyModule.Defuzzify("desirability");

            return result;
        }
    }
}
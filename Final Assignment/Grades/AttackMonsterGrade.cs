﻿using Final_Assignment.Entities;
using Final_Assignment.Fuzzy;

namespace Final_Assignment.Evaluators
{
    public class AttackMonsterGrade : CharacterGrade
    {
        /*===========================================================================
         Constructors
         ===========================================================================*/

        public AttackMonsterGrade(Character character)
            : base(character)
        {
            FzSet lowAttack;
            FzSet mediumAttack;
            FzSet highAttack;

            FuzzyVariable = FuzzyModule.CreateFLV("attack");

            lowAttack = FuzzyVariable.AddLeftShoulderSet("low", 0, 50, 75);
            mediumAttack = FuzzyVariable.AddTriangularSet("medium", 25, 75, 50);
            highAttack = FuzzyVariable.AddRightShoulderSet("high", 50, 100, 75);

            FuzzyModule.AddRule(new FzVery(lowAttack), VeryDesirable);
            FuzzyModule.AddRule(new FzVery(mediumAttack), Desirable);
            FuzzyModule.AddRule(new FzVery(highAttack), Undesirable);
        }

        /*===========================================================================
         Methods
         ===========================================================================*/

        public override float GetDesirability()
        {
            float result;

            FuzzyVariable.Fuzzify(Character.Exhausted);
            result = FuzzyModule.Defuzzify("desirability");

            return result;
        }
    }
}
﻿using Final_Assignment.Entities;
using Final_Assignment.Fuzzy;

namespace Final_Assignment.Evaluators
{
    public abstract class CharacterGrade
    {
        /*===========================================================================
         Variables
         ===========================================================================*/
        protected Character Character;
        protected FuzzyVariable Desirability;
        protected FzSet Desirable;
        protected FuzzyModule FuzzyModule = new FuzzyModule();
        protected FuzzyVariable FuzzyVariable = new FuzzyVariable();
        protected FzSet Undesirable;
        protected FzSet VeryDesirable;
        /*===========================================================================
         Constructors
         ===========================================================================*/

        protected CharacterGrade(Character character)
        {
            Character = character;

            Desirability = FuzzyModule.CreateFLV("desirability");
            SetFuzzySet();
        }

        protected virtual void SetFuzzySet()
        {
            Undesirable = Desirability.AddLeftShoulderSet("undesirable", 0, 50, 25);
            Desirable = Desirability.AddTriangularSet("desirable", 25, 75, 50);
            VeryDesirable = Desirability.AddRightShoulderSet("veryDesirable", 50, int.MaxValue, 75);
        }

        /*===========================================================================
         Abstract methods
         ===========================================================================*/
        public abstract float GetDesirability();
    }
}
﻿using System.Collections.Generic;
using Final_Assignment.Entities;
using Final_Assignment.Properties;

namespace Final_Assignment
{
    public class World
    {
        /*=================================================================================
         _variables
         =================================================================================*/
        public readonly int WorldHeight = Resources.Background.Height;
        public readonly int WorldWidth = Resources.Background.Width;
        /*=================================================================================
         Constructors
         =================================================================================*/

        public World()
        {
            //Create game walls
            Walls = new List<Wall>
            {
                //Top
                new Wall(0, 0, WorldWidth, 0),
                //Bottom
                new Wall(WorldWidth, WorldHeight, 0, WorldHeight),
                //Left
                new Wall(0, WorldHeight, 0, 0),
                //Right
                new Wall(WorldWidth, 0, WorldWidth, WorldHeight)
            };

            Character = new Character(new Vector2D(0, 0));

            var monster1 = new RedDragon(new Vector2D(300, 300));
            var monster2 = new BlueDragon(new Vector2D(0, 100));
            var monster3 = new BlackDragon(new Vector2D(100, 200));
            var monster4 = new GreenBird(new Vector2D(0, 300));
            var monster5 = new PurpleBird(new Vector2D(200, 200));

            monster1.Target = Character;
            monster3.Target = Character;
            monster4.Target = monster5;
            monster5.Target = monster4;

            var obstacle1 = new Cloud(new Vector2D(300, 300));
            var obstacle2 = new Cloud(new Vector2D(400, 400));
            var obstacle3 = new Cloud(new Vector2D(500, 500));
            var obstacle4 = new Cloud(new Vector2D(600, 600));

            var magicSpot1 = new MagicSpot(new Vector2D(1700, 100));
            var magicSpot2 = new MagicSpot(new Vector2D(100, 900));

            Monsters = new List<MovingEntity>
            {
                monster1,
                monster2,
                monster3,
                monster4,
                monster5
            };

            Obstacles = new List<BaseGameEntity>
            {
                obstacle1,
                obstacle2,
                obstacle3,
                obstacle4,
                magicSpot1,
                magicSpot2
            };

            MagicSpots = new List<BaseGameEntity>
            {
                magicSpot1,
                magicSpot2
            };

            DrawableEntities = new List<BaseGameEntity>
            {
                Character,
                monster1,
                monster2,
                monster3,
                monster4,
                monster5,
                obstacle1,
                obstacle2,
                obstacle3,
                obstacle4,
                magicSpot1,
                magicSpot2
            };

            MovingEntities = new List<MovingEntity>
            {
                Character,
                monster1,
                monster2,
                monster3,
                monster4,
                monster5
            };

            //Create graph
            Graph = new Graph.Graph(WorldWidth, WorldHeight, 50, Obstacles);
        }

        /*=================================================================================
         Properties
         =================================================================================*/
        public Character Character { get; private set; }
        public List<MovingEntity> Monsters { get; private set; }
        public List<MovingEntity> MovingEntities { get; private set; }
        public List<BaseGameEntity> Obstacles { get; private set; }
        public List<BaseGameEntity> DrawableEntities { get; private set; }
        public List<BaseGameEntity> MagicSpots { get; private set; }
        public List<Wall> Walls { get; private set; }
        public Graph.Graph Graph { get; set; }
    }
}
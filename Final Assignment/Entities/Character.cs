﻿using System.Drawing;
using Final_Assignment.Goals;
using Final_Assignment.Properties;

namespace Final_Assignment.Entities
{
    public class Character : MovingEntity
    {
        /*==========================================================================
        Variables
        ==========================================================================*/
        public float Gold = 0;
        public float Exhausted = 0;
        /*==========================================================================
         constructors
         ==========================================================================*/

        public Character(Vector2D positon, double scale = 1)
            : base(positon,
                new Vector2D(1, 1), //Heading. 
                1000, //mass.
                0.2f, //max speed.
                100, //max force.
                100, //max turn rate.
                new Bitmap(Resources.Character, //Model
                    new Size(
                        (int) (Resources.Character.Width*scale),
                        (int) (Resources.Character.Height*scale))
                    ),
                scale) //Scale
        {
            Goals = new CharacterGoals(this);
        }

        /*==========================================================================
        Properties
        ==========================================================================*/
        public CharacterGoals Goals { get; private set; }
        /*==========================================================================
         Public methods
         ==========================================================================*/

        public void MoveTo(int x, int y)
        {
            Goals.Terminate();
            Goals.Enqueue(new SeekPath(this, x, y));
        }

        /*==========================================================================
         Public override methods
         ==========================================================================*/

        public override void Update(int timeElapsed)
        {
            Goals.Process();
            base.Update(timeElapsed);
        }
    }
}
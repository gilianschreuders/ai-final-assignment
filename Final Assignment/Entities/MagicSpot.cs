﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Final_Assignment.Properties;

namespace Final_Assignment.Entities
{
    public class MagicSpot : BaseGameEntity
    {
        public MagicSpot(Vector2D positon, double scale = 1)
            : base(
                positon,
                new Bitmap(Resources.magic_spot,
                    new Size((int)(Resources.magic_spot.Width * scale), (int)(Resources.magic_spot.Height * scale))), scale)
        {
        }
    }
}

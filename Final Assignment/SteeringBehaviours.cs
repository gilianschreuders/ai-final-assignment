﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Final_Assignment.Graph;

namespace Final_Assignment.Entities
{
    public class SteeringBehaviours
    {
        private int _exploreCount;
        /*================================================================
         Constructors
         ================================================================*/

        public SteeringBehaviours(MovingEntity entity)
        {
            Entity = entity;
            SeekOn = false;
            FleeOn = false;
            WanderOn = false;
            WallAvoidanceOn = false;
            SeekPathOn = false;
            ExploreOn = false;
            ArriveOn = false;
            Path = new Stack<Node>();
        }

        /*================================================================
         Properties
         ================================================================*/
        public MovingEntity Entity { get; private set; }
        public Stack<Node> Path { get; set; }
        public bool SeekOn { get; set; }
        public bool FleeOn { get; set; }
        public bool WanderOn { get; set; }
        public bool WallAvoidanceOn { get; set; }
        public bool SeekPathOn { get; set; }
        public bool ExploreOn { get; set; }
        public bool ArriveOn { get; set; }
        /*================================================================
         Private methods
         ================================================================*/

        //Steering behaviours________________________________________________________________________
        private Vector2D Explore()
        {
            if (!Game.World.Graph.Map.ContainsKey(_exploreCount))
            {
                _exploreCount++;
            }

            if (_exploreCount > Game.World.Graph.Map.Count)
            {
                _exploreCount = 0;
            }

            if (Entity.Position.Intersects(Game.World.Graph[_exploreCount].Position, 50))
            {
                _exploreCount++;
            }

            return Seek(Game.World.Graph[_exploreCount].Position);
        }

        private Vector2D Arrive(Vector2D target)
        {
            const float decelerationTweaker = 0.03f;

            var toTarget = target - Entity.Position;
            var distance = (float) toTarget.Length();

            float speed;

            Vector2D desiredVelocity;

            if (distance <= 5)
            {
                return new Vector2D();
            }

            speed = distance/decelerationTweaker;
            speed = (float) Math.Min(speed, Entity.MaxSpeed);

            desiredVelocity = toTarget*speed/distance;

            return desiredVelocity;
        }

        private Vector2D SeekPath()
        {
            Vector2D nearestPosition;
            Vector2D desiredVelocity;
            double distance;

            if (Path.Count == 0) return new Vector2D();

            nearestPosition = Path.Peek().Position;
            distance = (nearestPosition - Entity.Position).Length();
            desiredVelocity = Seek(nearestPosition);

            if (distance < 20)
            {
                Path.Pop();
            }

            return desiredVelocity;
        }

        private Vector2D Seek(Vector2D target)
        {
            var desiredVelocity = Vector2D.Normalize(target - Entity.Position)*Entity.MaxSpeed;
            return desiredVelocity - Entity.Velocity;
        }

        private Vector2D Flee(Vector2D target)
        {
            var desiredVelocity = Vector2D.Normalize(Entity.Position - target)*Entity.MaxSpeed;
            return desiredVelocity - Entity.Velocity;
        }

        private Vector2D Wander()
        {
            throw new NotImplementedException("Wanders is not finished yet!");

            const double radius = 20;
            const double wanderDistance = 2;
            const double jitter = 5;
            var random = new Random();
            var wanderTarget = new Vector2D(0, 0);
            Vector2D targetLocal;
            Vector2D targetWorld;

            wanderTarget += new Vector2D(random.Next(-1, 1)*jitter, random.Next(-1, 1)*jitter);
            wanderTarget.Normalize();
            wanderTarget *= radius;

            targetLocal = wanderTarget + new Vector2D(wanderDistance, 0);

            targetWorld = new Vector2D(0, 0); //TODO: fix this

            return targetWorld - Entity.Position;
        }

        private Vector2D WallAvoidance(List<Wall> walls)
        {
            //Variables
            const double feelerLength = 90;

            Vector2D temporary;
            Vector2D point = null;
            Vector2D closestPoint = null;
            Vector2D overshoot = null;

            Wall closestWall = null;

            var steeringForce = new Vector2D(0, 0);
            var feelers = new Vector2D[3];
            var currentDistance = 0.0;
            var closestDistance = double.MaxValue;

            //Logic

            #region Create feelers

            feelers[0] = Entity.Position + feelerLength*Entity.Heading;

            temporary = Entity.Heading;
            temporary.Rotate((Math.PI/2)*3.5);
            feelers[1] = Entity.Position + (feelerLength/2.0)*temporary;

            temporary = Entity.Heading;
            temporary.Rotate((Math.PI/2)*0.5);
            feelers[2] = Entity.Position + (feelerLength/2.0)*temporary;

            #endregion

            foreach (var feeler in feelers)
            {
                foreach (var wall in walls)
                {
                    if (LineIntersection2D(Entity.Position, feeler, wall.A, wall.B, ref currentDistance, ref point))
                    {
                        if (currentDistance < closestDistance)
                        {
                            closestDistance = currentDistance;
                            closestWall = wall;
                            closestPoint = point;
                        }
                    }
                } //Next wall

                if (closestWall != null)
                {
                    overshoot = feeler - closestPoint;
                    steeringForce = closestWall.GetNormal()*overshoot.Length();
                }
            } //Next feeler

            return steeringForce;
        }

        //Other____________________________________________________________________________________________________

        private bool LineIntersection2D(Vector2D a, Vector2D b, Vector2D c, Vector2D d, ref double distance,
            ref Vector2D point)
        {
            var rTop = (a.Y - c.Y)*(d.X - c.X) - (a.X - c.X)*(d.Y - c.Y);
            var rBot = (b.X - a.X)*(d.Y - c.Y) - (b.Y - a.Y)*(d.X - c.X);

            var sTop = (a.Y - c.Y)*(b.X - a.X) - (a.X - c.X)*(b.Y - a.Y);
            var sBot = (b.X - a.X)*(d.Y - c.Y) - (b.Y - a.Y)*(d.X - c.X);

            if ((rBot == 0) || (sBot == 0))
            {
                //lines are parallel
                return false;
            }

            var r = rTop/rBot;
            var s = sTop/sBot;

            if ((r > 0) && (r < 1) && (s > 0) && (s < 1))
            {
                distance = Vector2D.Distance(a, b)*r;

                point = a + r*(b - a);

                return true;
            }

            distance = 0;

            return false;
        }

        //Calculates___________________________________________________

        private Vector2D TargetCalculate(Vector2D target)
        {
            var steeringForce = new Vector2D();

            if (SeekOn)
                steeringForce += Seek(target);
            if (FleeOn)
                steeringForce += Flee(target);
            if (ArriveOn)
                steeringForce += Arrive(target);

            return steeringForce + TargetlessCalculate();
        }

        private Vector2D TargetlessCalculate()
        {
            var steeringForce = new Vector2D();

            if (WanderOn)
                steeringForce += Wander();
            if (WallAvoidanceOn)
                steeringForce += WallAvoidance(Game.World.Walls);
            if (SeekPathOn)
                steeringForce += SeekPath();
            if (ExploreOn)
                steeringForce += Explore();

            return steeringForce;
        }

        /*================================================================
         Public methods
         ================================================================*/

        public void DrawPath(Graphics graphics)
        {
            if (Path.Count == 0) return;

            var path = Path;

            try
            {
                foreach (var node in path)
                {
                    node.Draw(graphics, Color.Black, 4);

                    if (node.Previous == null) return;

                    Edge.Draw(graphics, Color.Black, node.Position, node.Previous.Position, 2);
                }
            }
            catch (InvalidOperationException e)
            {
            }
        }

        public Vector2D Calculate(BaseGameEntity target)
        {
            if (target == null)
                return TargetlessCalculate();

            return TargetCalculate(target.Position);
        }

        public Vector2D Calculate(Vector2D target)
        {
            return TargetCalculate(target);
        }
    }
}
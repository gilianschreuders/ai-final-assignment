﻿namespace Final_Assignment.Goals
{
    public enum GoalStatus
    {
        Complete,
        Active,
        Terminated,
        Inactive
    }
}
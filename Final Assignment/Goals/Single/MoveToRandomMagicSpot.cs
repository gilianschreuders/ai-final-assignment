﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class MoveToRandomMagicSpot : GoalComponent
    {
        public MoveToRandomMagicSpot(MovingEntity entity) : base(entity)
        {
        }

        public override void Activate()
        {
            var magicSpotCount = Game.World.MagicSpots.Count;
            var randomMagicSpot = Game.World.MagicSpots[Game.GameRandom.Next(magicSpotCount)];
            Entity.SteeringBehaviours.SeekOn = true;
            Entity.Target = randomMagicSpot;
            Status = GoalStatus.Active;
        }

        public override void Terminate()
        {
            Entity.SteeringBehaviours.SeekOn = false;
            Status = GoalStatus.Complete;
        }

        public override void Process()
        {
            if (Status == GoalStatus.Inactive)
            {
                Activate();
            }

            if (Entity.Position.Intersects(Entity.Target.Position, 25))
            {
                Terminate();
            }
        }

        public override string ToString()
        {
            return "Move to random magic spot";
        }
    }
}
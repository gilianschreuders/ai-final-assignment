﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    internal class SeekPath : GoalComponent
    {
        private readonly int currentNode;
        private readonly int targetNode;

        public SeekPath(MovingEntity entity, int x, int y) : base(entity)
        {
            currentNode = Game.World.Graph.GetNode(Entity.Position.X, Entity.Position.Y, Game.World.Graph.Spacing);
            targetNode = Game.World.Graph.GetNode(x, y, Game.World.Graph.Spacing/2);
        }

        public override void Activate()
        {
            Status = GoalStatus.Active;

            var path = Game.World.Graph.GetPath(currentNode, targetNode, true);

            Entity.SteeringBehaviours.Path = path;
            Entity.SteeringBehaviours.SeekOn = false;
            Entity.SteeringBehaviours.SeekPathOn = true;
        }

        public override void Terminate()
        {
            Entity.SteeringBehaviours.SeekPathOn = false;
            Status = GoalStatus.Complete;
        }

        public override void Process()
        {
            if(Status == GoalStatus.Inactive)
                Activate();

            if (Entity.SteeringBehaviours.Path.Count == 0)
            {
                Terminate();
            }
        }

        public override string ToString()
        {
            return "Seeking path";
        }
    }
}
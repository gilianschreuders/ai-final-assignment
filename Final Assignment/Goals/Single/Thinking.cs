﻿using System.Collections.Generic;
using System.Linq;
using Final_Assignment.Entities;
using Final_Assignment.Evaluators;

namespace Final_Assignment.Goals
{
    public class Thinking : GoalComponent
    {
        /*===================================================================================
         Variables
         ===================================================================================*/
        private readonly AttackMonsterGrade _attackGrade;
        private readonly BankGoldGrade _bankGrade;
        private readonly CharacterGoals _characterGoals;
        private readonly SortedList<float, GoalComponent> _grades = new SortedList<float, GoalComponent>();
        private readonly RecoverGrade _recoverGrade;
        private int _countdown = 25;
        /*===================================================================================
         Constructors
         ===================================================================================*/

        public Thinking(MovingEntity entity, CharacterGoals characterGoals) : base(entity)
        {
            _characterGoals = characterGoals;

            _recoverGrade = new RecoverGrade(characterGoals.Character);
            _bankGrade = new BankGoldGrade(characterGoals.Character);
            _attackGrade = new AttackMonsterGrade(characterGoals.Character);
        }

        /*===================================================================================
         Private methods
         ===================================================================================*/

        private void CalculateGoal()
        {
            var attackDesirability = _attackGrade.GetDesirability();
            var bankDesirability = _bankGrade.GetDesirability();
            var recoverDesirability = _recoverGrade.GetDesirability();

            _grades.Clear();

            if (!_grades.ContainsKey(attackDesirability))
                _grades.Add(attackDesirability, new HuntMonster(_characterGoals.Character));
            if (!_grades.ContainsKey(bankDesirability))
                _grades.Add(bankDesirability, new DepositGoldInBank(_characterGoals.Character));
            if (!_grades.ContainsKey(recoverDesirability))
                _grades.Add(recoverDesirability, new RecoverAtMagicSpot(_characterGoals.Character));
        }

        /*===================================================================================
         Override methods
         ===================================================================================*/

        public override void Activate()
        {
            Status = GoalStatus.Active;
            Entity.SteeringBehaviours.SeekOn = true;
        }

        public override void Terminate()
        {
            Status = GoalStatus.Complete;
            Entity.SteeringBehaviours.SeekOn = false;
        }

        public override void Process()
        {
            if (Status == GoalStatus.Inactive)
                Activate();

            if (_countdown > 0)
            {
                _countdown--;
                return;
            }

            CalculateGoal();

            _characterGoals.Goals.Enqueue(_grades.Last().Value);

            Terminate();
        }

        public override string ToString()
        {
            return string.Format("Thinking: {0}", _countdown);
        }
    }
}
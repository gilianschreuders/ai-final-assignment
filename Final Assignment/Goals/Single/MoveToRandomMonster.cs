﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class MoveToRandomMonster : GoalComponent
    {
        public MoveToRandomMonster(MovingEntity entity) : base(entity)
        {
        }

        public override void Activate()
        {
            var monsterCount = Game.World.Monsters.Count;
            var randomMonster = Game.World.Monsters[Game.GameRandom.Next(monsterCount)];

            Entity.SteeringBehaviours.SeekOn = true;
            Entity.Target = randomMonster;

            Status = GoalStatus.Active;
        }

        public override void Terminate()
        {
            Entity.SteeringBehaviours.SeekOn = false;
            Status = GoalStatus.Complete;
        }

        public override void Process()
        {
            if (Status != GoalStatus.Active)
            {
                Activate();
            }

            if (Entity.Position.Intersects(Entity.Target.Position, 10))
            {
                Terminate();
            }
        }

        public override string ToString()
        {
            return "Move to random monster";
        }
    }
}
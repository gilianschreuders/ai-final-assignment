﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class Recover : StayAtTarget
    {
        private readonly Character _character;

        public Recover(Character entity, int time)
            : base(entity, time)
        {
            _character = entity;
        }

        public override void Process()
        {
            if (_character.Exhausted > 0)
            {
                _character.Exhausted -= 1;
            }

            if (_character.Exhausted < 0)
            {
                _character.Exhausted = 0;
            }

            base.Process();
        }

        public override string ToString()
        {
            return string.Format("Recovering Exhaustion: {0}", Time);
        }
    }
}

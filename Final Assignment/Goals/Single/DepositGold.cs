﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class DepositGold : StayAtTarget
    {
        private readonly Character _character;

        public DepositGold(Character entity, int time)
            : base(entity, time)
        {
            _character = entity;
        }

        public override void Process()
        {
            _character.Gold -= 50f;

            if (_character.Gold < 0)
                _character.Gold = 0;


            base.Process();
        }

        public override string ToString()
        {
            return string.Format("Dumping gold: {0}", Time);
        }
    }
}
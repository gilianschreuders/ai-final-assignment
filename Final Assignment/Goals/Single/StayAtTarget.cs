﻿using System;
using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class StayAtTarget : GoalComponent
    {
        protected int Time;

        public StayAtTarget(MovingEntity entity, int time) : base(entity)
        {
            Time = time;
        }

        public override void Activate()
        {
            Entity.SteeringBehaviours.SeekOn = true;
            Status = GoalStatus.Active;
        }

        public override void Terminate()
        {
            Entity.SteeringBehaviours.SeekOn = false;
            Status = GoalStatus.Complete;
        }

        public override void Process()
        {
            if (Status == GoalStatus.Inactive) Activate();
            if (Entity.Target == null) throw new NullReferenceException("Target is null");
            if (Time > 0) Time--;
            if (Time <= 0) Terminate();
        }

        public override string ToString()
        {
            return string.Format("Stay at target: {0}", Time);
        }
    }
}
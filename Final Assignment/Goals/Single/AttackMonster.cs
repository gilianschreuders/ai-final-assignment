﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class AttackMonster : StayAtTarget
    {
        private readonly Character _character;

        public AttackMonster(Character entity, int time) : base(entity, time)
        {
            _character = entity;
        }

        public override void Process()
        {
            _character.Gold += 13.37f;

            base.Process();
        }

        public override string ToString()
        {
            return string.Format("Attacking moster: {0}", Time);
        }
    }
}
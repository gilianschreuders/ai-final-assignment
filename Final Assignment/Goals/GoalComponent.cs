﻿using System.Drawing;
using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public abstract class GoalComponent
    {
        private readonly SolidBrush _drawBrush = new SolidBrush(Color.Orange);
        private readonly Font _drawFont = new Font("Arial", 16);

        protected GoalComponent(MovingEntity entity)
        {
            Entity = entity;
            Status = GoalStatus.Inactive;
        }

        public MovingEntity Entity { get; private set; }
        public GoalStatus Status { get; protected set; }
        public abstract void Activate();
        public abstract void Terminate();
        public abstract void Process();
        public abstract override string ToString();

        public void Draw(Graphics graphics)
        {
            graphics.DrawString(ToString(), _drawFont, _drawBrush, (Entity.Position).ToPointF());
        }
    }
}
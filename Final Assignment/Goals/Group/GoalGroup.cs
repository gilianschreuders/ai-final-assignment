﻿using System.Collections.Generic;
using System.Linq;
using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public abstract class GoalGroup : GoalComponent
    {
        /*================================================================
         _variables
         ================================================================*/
        public Queue<GoalComponent> Goals = new Queue<GoalComponent>();
        /*================================================================
         Constructors
         ================================================================*/

        protected GoalGroup(MovingEntity entity) : base(entity)
        {
        }

        /*================================================================
         Public methods
         ================================================================*/

        public void Enqueue(GoalComponent goalComponent)
        {
            Goals.Enqueue(goalComponent);
        }

        public void ClearGoals()
        {
            Terminate();
            Goals = new Queue<GoalComponent>();
        }

        /*================================================================
         Override methods
         ================================================================*/

        public override void Activate()
        {
            if (Goals.Count == 0)
            {
                Status = GoalStatus.Complete;
            }
            else
            {
                Status = GoalStatus.Active;
            }
        }

        public override void Terminate()
        {
            Status = GoalStatus.Complete;
            foreach (var goal in Goals) goal.Terminate();
            Goals = new Queue<GoalComponent>();
        }

        public override void Process()
        {
            if (Status == GoalStatus.Inactive) Activate();

            if (Goals.Count == 0)
            {
                Terminate();
                return;
            }

            Goals.Peek().Process();

            if (Goals.Peek().Status == GoalStatus.Complete) Goals.Dequeue();
        }
    }
}
﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class CharacterGoals : GoalGroup
    {
        public Character Character;

        public CharacterGoals(Character entity) : base(entity)
        {
            Character = entity;
            Enqueue(new Thinking(entity, this));
        }

        public override void Process()
        {
            if (Goals.Count == 0)
            {
                Goals.Enqueue(new Thinking(Entity, this));
            }

            base.Process();
        }

        public override string ToString()
        {
            var result = string.Format("Exhausted:{0} \nGold:{1} \nCharacter \n", Character.Exhausted, Character.Gold);

            foreach (var goal in Goals) result += string.Format("{0}\n", goal);

            return result;
        }
    }
}
﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class HuntMonster : GoalGroup
    {
        private readonly Character _character;

        public HuntMonster(Character entity) : base(entity)
        {
            _character = entity;
            Enqueue(new MoveToRandomMonster(entity));
            Enqueue(new AttackMonster(entity, 50));
        }

        public override void Process()
        {
            _character.Exhausted += 0.1f;

            if (_character.Exhausted < 0)
                _character.Exhausted = 0;

            base.Process();
        }

        public override string ToString()
        {
            var result = "Attacking monsters: \n";

            foreach (var goal in Goals) result += string.Format("{0}\n", goal);

            return result;
        }
    }
}
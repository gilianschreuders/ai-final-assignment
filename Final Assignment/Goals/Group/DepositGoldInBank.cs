﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class DepositGoldInBank : GoalGroup
    {
        public DepositGoldInBank(Character entity) : base(entity)
        {
            Enqueue(new MoveToRandomMagicSpot(entity));
            Enqueue(new DepositGold(entity, 75));
        }

        public override string ToString()
        {
            var result = "Stash gold to bank: \n";

            foreach (var goal in Goals) result += string.Format("{0}\n", goal);

            return result;
        }
    }
}

﻿using Final_Assignment.Entities;

namespace Final_Assignment.Goals
{
    public class RecoverAtMagicSpot : GoalGroup
    {
        public RecoverAtMagicSpot(Character entity) : base(entity)
        {
            Enqueue(new MoveToRandomMagicSpot(entity));
            Enqueue(new Recover(entity, 80));
        }

        public override string ToString()
        {
            var result = "Recover at magic spot: \n";

            foreach (var goal in Goals) result += string.Format("{0}\n", goal);

            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Final_Assignment.Fuzzy.Sets;

namespace Final_Assignment.Fuzzy
{
    public class FuzzyVariable
    {
        public FuzzyVariable()
        {
            Sets = new Dictionary<string, FuzzySet>();
        }

        public Dictionary<string, FuzzySet> Sets { get; set; }
        public float Min { get; set; }
        public float Max { get; set; }

        public FzSet AddTriangularSet(string name, float min, float max, float peak)
        {
            var triangleSet = new TriangleSet(min, max, peak);
            Sets.Add(name, triangleSet);
            AdjustRangeToFit(min, max);
            return new FzSet(triangleSet);
        }

        public FzSet AddRightShoulderSet(string name, float min, float max, float peak)
        {
            var rightShoulderSet = new RightShoulderSet(min, max, peak);
            Sets.Add(name, rightShoulderSet);
            AdjustRangeToFit(min, max);
            return new FzSet(rightShoulderSet);
        }

        public FzSet AddLeftShoulderSet(string name, float min, float max, float peak)
        {
            var leftShoulderSet = new LeftShoulderSet(min, max, peak);
            Sets.Add(name, leftShoulderSet);
            AdjustRangeToFit(min, max);
            return new FzSet(leftShoulderSet);
        }

        public FzSet AddSingletonSet(string name, float min, float max, float peak)
        {
            var singletonSet = new SingletonSet(min, max, peak);
            Sets.Add(name, singletonSet);
            AdjustRangeToFit(min, max);
            return new FzSet(singletonSet);
        }

        public void Fuzzify(float value)
        {
            if (value < Max || value > Min)
            {
                throw new ArgumentOutOfRangeException(string.Format("Value: {0} is not in range of {1} and {2}", value,
                    Max, Min));
            }

            foreach (var set in Sets.Values)
            {
                set.DOM = set.CalculateDOM(value);
            }
        }

        public float DefuzzifyMaxAv()
        {
            var bottom = 0f;
            var top = 0f;

            foreach (var set in Sets.Values)
            {
                bottom += set.DOM;
                top += set.RepresentativeValue*set.DOM;
            }

            if (bottom == 0)
            {
                return 0;
            }

            return top/bottom;
        }

        private void AdjustRangeToFit(float min, float max)
        {
            Max = Math.Min(Max, min);
            Min = Math.Max(Min, max);
        }
    }
}
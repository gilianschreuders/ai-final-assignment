﻿namespace Final_Assignment.Fuzzy
{
    public class FuzzyRule
    {
        public FuzzyRule(FuzzyTerm antecedent, FuzzyTerm consequence)
        {
            Antecedent = antecedent;
            Consequence = consequence;
        }

        public FuzzyTerm Antecedent { get; private set; }
        public FuzzyTerm Consequence { get; private set; }

        public void Calculate()
        {
            Consequence.ORwithDOM(Antecedent.GetDOM());
        }

        public void SetConfidenceOfConsequentToZero()
        {
            Consequence.ClearDOM();
        }
    }
}
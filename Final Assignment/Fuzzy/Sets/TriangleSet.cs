﻿namespace Final_Assignment.Fuzzy.Sets
{
    public class TriangleSet : FuzzySet
    {
        public TriangleSet(float leftOffset, float rightOffset, float peakPoint)
            : base(peakPoint)
        {
            PeakPoint = peakPoint;
            LeftOffset = leftOffset;
            RightOffset = rightOffset;
        }

        public override float CalculateDOM(float value)
        {
            if (RightOffset == 0.0f && PeakPoint == value ||
                LeftOffset == 0.0f && PeakPoint == value)
            {
                return 1.0f;
            }

            if (value <= PeakPoint && value >= (PeakPoint + LeftOffset))
            {
                var grad = 1.0f/LeftOffset;
                return grad*(value - PeakPoint - LeftOffset);
            }

            if (value > PeakPoint && value < PeakPoint + RightOffset)
            {
                var grad = 1.0f/-RightOffset;
                return grad*(value - PeakPoint) + 1.0f;
            }

            return 0.0f;
        }
    }
}
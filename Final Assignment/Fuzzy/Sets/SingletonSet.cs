﻿namespace Final_Assignment.Fuzzy.Sets
{
    public class SingletonSet : FuzzySet
    {
        public SingletonSet(float leftOffset, float rightOffset, float peakPoint)
            : base(peakPoint)
        {
            PeakPoint = peakPoint;
            LeftOffset = leftOffset;
            RightOffset = rightOffset;
        }

        public override float CalculateDOM(float value)
        {
            if ((value >= PeakPoint - LeftOffset) &&
                (value <= PeakPoint + RightOffset))
            {
                return 1.0f;
            }

            return 0.0f;
        }
    }
}
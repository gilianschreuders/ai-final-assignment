﻿namespace Final_Assignment.Fuzzy.Sets
{
    public class LeftShoulderSet : FuzzySet
    {
        public LeftShoulderSet(float leftOffset, float rightOffset, float peakPoint)
            : base(((peakPoint - leftOffset) + peakPoint)/2)
        {
            PeakPoint = peakPoint;
            LeftOffset = leftOffset;
            RightOffset = rightOffset;
        }

        public override float CalculateDOM(float value)
        {
            if (RightOffset == 0.0f && PeakPoint == value ||
                LeftOffset == 0.0f && PeakPoint == value)
            {
                return 1.0f;
            }

            if (value >= PeakPoint && value < (PeakPoint + RightOffset))
            {
                var grad = 1.0f/-RightOffset;
                return grad*(value - PeakPoint) + 1.0f;
            }

            if (value < PeakPoint && value >= PeakPoint - LeftOffset)
            {
                return 1.0f;
            }

            return 0.0f;
        }
    }
}
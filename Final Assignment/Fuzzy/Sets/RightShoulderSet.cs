﻿namespace Final_Assignment.Fuzzy.Sets
{
    public class RightShoulderSet : FuzzySet
    {
        public RightShoulderSet(float leftOffset, float rightOffset, float peakPoint)
            : base(((peakPoint + rightOffset) + peakPoint)/2)
        {
            PeakPoint = peakPoint;
            LeftOffset = leftOffset;
            RightOffset = rightOffset;
        }

        public override float CalculateDOM(float value)
        {
            if (RightOffset == 0.0f && PeakPoint == value ||
                LeftOffset == 0.0f && PeakPoint == value)
            {
                return 1.0f;
            }

            if (value <= PeakPoint && value > (PeakPoint - LeftOffset))
            {
                var grad = 1.0f/LeftOffset;
                return grad*(value - (PeakPoint - LeftOffset));
            }

            if (value > PeakPoint && value <= PeakPoint + RightOffset)
            {
                return 1.0f;
            }

            return 0.0f;
        }
    }
}
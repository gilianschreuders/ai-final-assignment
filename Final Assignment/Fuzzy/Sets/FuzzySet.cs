﻿using System;

namespace Final_Assignment.Fuzzy.Sets
{
    public abstract class FuzzySet
    {
        private float _dom;

        protected FuzzySet(float representativeValue)
        {
            RepresentativeValue = representativeValue;
        }

        public float DOM
        {
            get { return _dom; }
            set
            {
                if (value < 0 || value > 1)
                {
                    throw new InvalidOperationException("Value must be between 0 and 1.");
                }

                _dom = value;
            }
        }

        public float RepresentativeValue { get; protected set; }
        public float LeftOffset { get; protected set; }
        public float PeakPoint { get; protected set; }
        public float RightOffset { get; protected set; }
        public abstract float CalculateDOM(float value);

        public void ClearDOM()
        {
            DOM = 0;
        }

        public void ORwithDOM(float value)
        {
            if (value > DOM) DOM = value;
        }
    }
}
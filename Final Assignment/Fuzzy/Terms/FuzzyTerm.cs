﻿namespace Final_Assignment.Fuzzy
{
    public abstract class FuzzyTerm
    {
        public FuzzyTerm Clone()
        {
            return this;
        }

        public abstract float GetDOM();
        public abstract void ClearDOM();
        public abstract void ORwithDOM(float value);

        public static FuzzyTerm operator &(FuzzyTerm left, FuzzyTerm right)
        {
            return new FzAnd(left, right);
        }

        public static FuzzyTerm operator |(FuzzyTerm left, FuzzyTerm right)
        {
            return new FzOr(left, right);
        }

        public static bool operator true(FuzzyTerm term)
        {
            return term != null;
        }

        public static bool operator false(FuzzyTerm term)
        {
            return term == null;
        }
    }
}
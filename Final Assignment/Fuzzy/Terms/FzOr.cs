﻿using System;
using System.Collections.Generic;

namespace Final_Assignment.Fuzzy
{
    public class FzOr : FuzzyTerm
    {
        public FzOr(FuzzyTerm left, FuzzyTerm right)
        {
            Terms = new List<FuzzyTerm> {left, right};
        }

        public List<FuzzyTerm> Terms { get; protected set; }

        public override float GetDOM()
        {
            var biggest = float.MinValue;

            foreach (var term in Terms)
            {
                if (term.GetDOM() > biggest)
                {
                    biggest = term.GetDOM();
                }
            }

            return biggest;
        }

        public override void ClearDOM()
        {
            throw new NotImplementedException();
        }

        public override void ORwithDOM(float value)
        {
            throw new NotImplementedException();
        }
    }
}
﻿namespace Final_Assignment.Fuzzy
{
    public class FzVery : FuzzyTerm
    {
        public FzVery(FzSet set)
        {
            Set = set;
        }

        public FzSet Set { get; set; }

        public override float GetDOM()
        {
            return Set.GetDOM()*Set.GetDOM();
        }

        public override void ClearDOM()
        {
            Set.ClearDOM();
        }

        public override void ORwithDOM(float value)
        {
            Set.ORwithDOM(value*value);
        }
    }
}
﻿using Final_Assignment.Fuzzy.Sets;

namespace Final_Assignment.Fuzzy
{
    public class FzSet : FuzzyTerm
    {
        public FzSet(FuzzySet set)
        {
            Set = set;
        }

        public FuzzySet Set { get; set; }

        public override float GetDOM()
        {
            return Set.DOM;
        }

        public override void ClearDOM()
        {
            Set.ClearDOM();
        }

        public override void ORwithDOM(float value)
        {
            Set.ORwithDOM(value);
        }
    }
}
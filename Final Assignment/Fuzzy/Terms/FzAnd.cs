﻿using System.Collections.Generic;

namespace Final_Assignment.Fuzzy
{
    public class FzAnd : FuzzyTerm
    {
        public FzAnd(FuzzyTerm left, FuzzyTerm right)
        {
            Terms = new List<FuzzyTerm> {left, right};
        }

        public List<FuzzyTerm> Terms { get; protected set; }

        public override float GetDOM()
        {
            var smallest = float.MaxValue;

            foreach (var term in Terms)
            {
                if (term.GetDOM() < smallest)
                {
                    smallest = term.GetDOM();
                }
            }

            return smallest;
        }

        public override void ClearDOM()
        {
            foreach (var term in Terms)
            {
                term.ClearDOM();
            }
        }

        public override void ORwithDOM(float value)
        {
            foreach (var term in Terms)
            {
                term.ORwithDOM(value);
            }
        }
    }
}
﻿using System;

namespace Final_Assignment.Fuzzy
{
    public class FzFairly : FuzzyTerm
    {
        public FzFairly(FzSet set)
        {
            Set = set;
        }

        public FzSet Set { get; set; }

        public override float GetDOM()
        {
            return (float) Math.Sqrt(Set.GetDOM());
        }

        public override void ClearDOM()
        {
            Set.ClearDOM();
        }

        public override void ORwithDOM(float value)
        {
            Set.ORwithDOM((float) Math.Sqrt(value));
        }
    }
}
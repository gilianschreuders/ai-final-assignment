﻿using System.Collections.Generic;

namespace Final_Assignment.Fuzzy
{
    public class FuzzyModule
    {
        public FuzzyModule()
        {
            Variables = new Dictionary<string, FuzzyVariable>();
            Rules = new List<FuzzyRule>();
        }

        public Dictionary<string, FuzzyVariable> Variables { get; set; }
        public List<FuzzyRule> Rules { get; set; }

        public FuzzyVariable CreateFLV(string name)
        {
            Variables[name] = new FuzzyVariable();
            return Variables[name];
        }

        public void AddRule(FuzzyTerm antecedent, FuzzyTerm consequence)
        {
            Rules.Add(new FuzzyRule(antecedent, consequence));
        }

        public void Fuzzify(string name, float value)
        {
            Variables[name].Fuzzify(value);
        }

        public float Defuzzify(string name)
        {
            SetConfidencesOfConsequentsToZero();
            foreach (var fuzzyRule in Rules)
            {
                fuzzyRule.Calculate();
            }

            return Variables[name].DefuzzifyMaxAv();
        }

        public void SetConfidencesOfConsequentsToZero()
        {
            foreach (var fuzzyRule in Rules)
            {
                fuzzyRule.SetConfidenceOfConsequentToZero();
            }
        }
    }
}
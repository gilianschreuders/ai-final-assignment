﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Final_Assignment.Entities;

namespace Final_Assignment.Graph
{
    public class Graph
    {
        /*=================================================================
         Constructors
         =================================================================*/
        //Basic
        public Graph(int spacing = 50)
        {
            Map = new Dictionary<int, Node>();
            Spacing = spacing;
        }

        //Fill graph
        public Graph(int width, int height, int spacing)
            : this(spacing)
        {
            InitializeNodes(width, height, spacing);
            InitializeEdges(width, height, spacing);
        }

        //Fill graph but not at a static position
        public Graph(int width, int height, int spacing, List<BaseGameEntity> obstacles)
            : this(spacing)
        {
            InitializeNodes(width, height, spacing);
            ClearNodes(obstacles);
            InitializeEdges(width, height, spacing);
        }

        /*=================================================================
         Indexers
         =================================================================*/

        public Node this[int id]
        {
            get { return Map[id]; }
        }

        /*=================================================================
         Properties
         =================================================================*/
        public Dictionary<int, Node> Map { get; private set; }
        public int Spacing { get; private set; }
        /*=================================================================
         Private methods
         =================================================================*/

        //Clear nodes where obstacles are.
        private void ClearNodes(List<BaseGameEntity> obstacles)
        {
            double nodeX;
            double nodeY;
            double entityX1;
            double entityX2;
            double entityY1;
            double entityY2;
            double entityWidth;
            double entityHeight;

            foreach (var obstacle in obstacles)
            {
                //Set entity size
                entityWidth = obstacle.Model.Width;
                entityHeight = obstacle.Model.Height;

                //Set X's
                entityX1 = obstacle.Position.X;
                entityX2 = obstacle.Position.X + entityWidth;

                //Set Y's
                entityY1 = obstacle.Position.Y;
                entityY2 = obstacle.Position.Y + entityHeight;

                foreach (var node in Map.Values.ToList())
                {
                    nodeX = node.Position.X;
                    nodeY = node.Position.Y;

                    if (nodeX >= entityX1 && nodeX <= entityX2 && //X
                        nodeY >= entityY1 && nodeY <= entityY2) //Y
                    {
                        Map.Remove(node.Id);
                    }
                }
            }
        }

        private void InitializeNodes(int width, int height, int spacing)
        {
            Node tempNode;
            var rowCount = height/spacing;
            var columnCount = width/spacing;

            for (var y = 0; y < rowCount; y++)
            {
                for (var x = 0; x < columnCount; x++)
                {
                    tempNode = new Node(x*spacing, y*spacing);
                    Map.Add(tempNode.Id, tempNode);
                }
            }
        }

        private void InitializeEdges(int width, int height, int spacing)
        {
            Node tempNode;
            int tempId;

            var rowCount = height/spacing;
            var columnCount = width/spacing;

            double nodeRow;
            double eastNodeRow;
            double westNodeRow;

            int northNodeRow;
            int southNodeRow;
            int southeastNodeRow;
            int southwestNodeRow;

            foreach (var node in Map.Values)
            {
                tempNode = node;
                tempId = node.Id;

                nodeRow = Math.Floor(tempId/(double) columnCount);
                northNodeRow = tempId - columnCount;
                eastNodeRow = Math.Floor((tempId + 1)/(double) columnCount);
                westNodeRow = Math.Floor((tempId - 1)/(double) columnCount);
                southNodeRow = tempId + columnCount;
                southeastNodeRow = tempId + columnCount + 1;
                southwestNodeRow = tempId + columnCount - 1;

                //Enqueue North
                try
                {
                    tempNode.AddEdge(Map[northNodeRow]);
                }
                catch (Exception)
                {
                    // Do not add.
                }

                //Enqueue East
                if (nodeRow == eastNodeRow)
                {
                    try
                    {
                        tempNode.AddEdge(Map[tempId + 1]);
                    }
                    catch (Exception)
                    {
                        // Do not add.
                    }
                }

                //Enqueue Southeast
                //                try
                //                {
                //                    tempNode.AddEdge(Map[southeastNodeRow);
                //                }
                //                catch (Exception)
                //                {
                //                    // Do not add.
                //                }

                //Enqueue South
                try
                {
                    tempNode.AddEdge(Map[southNodeRow]);
                }
                catch (Exception)
                {
                    // Do not add.
                }

                //Enqueue Southwest
                //                try
                //                {
                //                    tempNode.AddEdge(Map[southwestNodeRow);
                //                }
                //                catch (Exception)
                //                {
                //                    // Do not add.
                //                }

                //Enqueue West
                if (nodeRow == westNodeRow)
                {
                    try
                    {
                        tempNode.AddEdge(Map[tempId - 1]);
                    }
                    catch (Exception)
                    {
                        // Do not add.
                    }
                }
            }
        }

        private void ResetNodes()
        {
            foreach (var node in Map.Values)
            {
                node.Reset();
            }
        }

        private void Dijkstra(int id)
        {
            var queue = new Queue<Edge>();

            Node startNode;
            Node node1;
            Node node2;
            Edge edge1;
            int nodesSeen;
            double cost;

            if (!Map.ContainsKey(id)) return;

            ResetNodes();
            startNode = Map[id];
            startNode.Distance = 0;
            queue.Enqueue(new Edge(startNode, 0));

            nodesSeen = 0;
            while (queue.Count != 0 && nodesSeen < Map.Count)
            {
                edge1 = queue.Dequeue();
                node1 = edge1.Destination;

                if (node1.Scratch != 0)
                {
                    break;
                }

                node1.Scratch = 1;
                nodesSeen++;

                foreach (var edge in node1.Edges)
                {
                    node2 = edge.Destination;
                    cost = edge.Cost;

                    if (cost < 0)
                    {
                        throw new Exception("Graph ahs negative edges.");
                    }

                    if (node2.Distance > node1.Distance + cost)
                    {
                        node2.Distance = node1.Distance + cost;
                        node2.Previous = node1;
                        queue.Enqueue(new Edge(node2, node2.Distance));
                    }
                }
            }
        }

        private double GetHeuristic(int from, int to)
        {
            var positonA = Map[from].Position;
            var positonB = Map[to].Position;
            return Vector2D.Distance(positonA, positonB);
        }

        private void AStar(int from, int to)
        {
            var queue = new List<Edge>();
            Node startNode;
            Node node1;
            Node node2;
            Edge edge1;
            int nodesSeen;
            double gCost;
            double hCost;

            if (!Map.ContainsKey(from)) return; //No key found.

            ResetNodes();

            startNode = Map[from];
            startNode.Distance = 0;
            queue.Add(new Edge(startNode, 0));

            nodesSeen = 0;

            while (queue.Count != 0 && nodesSeen < Map.Count)
            {
                edge1 = queue[0];
                queue.RemoveAt(0);
                node1 = edge1.Destination;

                if (node1.Id == to) return;
                if (node1.Scratch != 0) break;

                node1.Scratch = 1;
                nodesSeen++;

                foreach (var edge in node1.Edges)
                {
                    node2 = edge.Destination;
                    gCost = edge.Cost;

                    if (gCost < 0) throw new Exception("Graph has negative edges.");

                    hCost = GetHeuristic(node2.Id, to);

                    if (!(node2.Distance > node1.Distance + gCost + hCost)) continue;

                    node2.Distance = node1.Distance + gCost + hCost;
                    node2.Previous = node1;
                    queue.Add(new Edge(node2, node2.Distance));
                    queue = queue.OrderBy(e => e.Cost).ToList();
                }
            }
        }

        /*=================================================================
         Public methods
         =================================================================*/

        public int GetNode(double x, double y, int correction = 5)
        {
            return GetNode((int) x, (int) y, correction);
        }

        public int GetNode(int x, int y, int correction = 5)
        {
            var position = new Vector2D(x, y);
            var id = int.MinValue;

            //correction values
            var topLeft = new Vector2D(x - correction, y - correction);
            var bottomRight = new Vector2D(x + correction, y + correction);

            foreach (var node in Map.Values)
            {
                if (node.Position >= topLeft && node.Position <= bottomRight)
                {
                    id = node.Id;
                }
            }

            return id;
        }

        public Stack<Node> GetPath(int from, int to, bool aStar = false)
        {
            var results = new Stack<Node>();
            Node currentNode;

            if (to == int.MinValue) return results;

            if (aStar)
            {
                AStar(from, to);
            }
            else
            {
                Dijkstra(from);
            }

            currentNode = Map[to];

            while (currentNode.Previous != null)
            {
                results.Push(currentNode);
                currentNode = currentNode.Previous;
            }

            return results;
        }

        public void Draw(Graphics graphics)
        {
            foreach (var node in Map.Values)
            {
                node.Draw(graphics, Color.Orange);

                foreach (var edge in node.Edges)
                {
                    edge.Draw(graphics, Color.Orange, node.Position);
                }
            }
        }

        public void DrawCalculations(Graphics graphics)
        {
            foreach (var node in Map.Values)
            {
                if (node.Scratch == 0) continue;

                node.Draw(graphics, Color.Purple);

                foreach (var edge in node.Edges)
                {
                    edge.Draw(graphics, Color.Purple, node.Position);
                }
            }
        }
    }
}
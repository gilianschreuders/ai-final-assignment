﻿using System.Drawing;
using Final_Assignment.Entities;

namespace Final_Assignment.Graph
{
    public class Edge
    {
        /*=================================================================
         Constructors
         =================================================================*/

        public Edge(Node destination, double cost)
        {
            Destination = destination;
            Cost = cost;
        }

        /*=================================================================
        Properties
        =================================================================*/
        public Node Destination { get; set; }
        public double Cost { get; set; }
        /*=================================================================
        Public Methods
        =================================================================*/

        public void Draw(Graphics graphics, Color color, Vector2D position)
        {
            var pen = new Pen(color);
            var dX = (int) Destination.Position.X;
            var dY = (int) Destination.Position.Y;
            graphics.DrawLine(pen, (int) position.X, (int) position.Y, dX, dY);
        }

        /*=================================================================
        Static methods
        =================================================================*/

        public static void Draw(Graphics graphics, Color color, Vector2D vector1, Vector2D vector2, int thickness = 1)
        {
            var pen = new Pen(color, thickness);
            graphics.DrawLine(pen, (int) vector1.X, (int) vector1.Y, (int) vector2.X, (int) vector2.Y);
        }
    }
}
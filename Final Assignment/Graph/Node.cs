﻿using System.Collections.Generic;
using System.Drawing;
using Final_Assignment.Entities;

namespace Final_Assignment.Graph
{
    public class Node
    {
        private static int _autoIncremental;

        public Node(int x, int y)
        {
            Reset();

            Id = _autoIncremental;
            _autoIncremental++;

            Position = new Vector2D(x, y);

            Edges = new List<Edge>();
        }

        public int Id { get; private set; }
        public List<Edge> Edges { get; private set; }
        public double Distance { get; set; }
        public Node Previous { get; set; }
        public int Scratch { get; set; }
        public Vector2D Position { get; set; }

        public void Reset()
        {
            Distance = double.MaxValue;
            Previous = null;
            Scratch = 0;
        }

        public void AddEdge(Node destination)
        {
            Edges.Add(new Edge(destination, 1));
        }

        public void Draw(Graphics graphics, Color color, int thickness = 3)
        {
            var dotPen = new Pen(color, thickness);
            var x = (int) Position.X;
            var y = (int) Position.Y;
            var rect = new Rectangle(x, y, 3, 3);
            graphics.DrawEllipse(dotPen, rect);
        }
    }
}